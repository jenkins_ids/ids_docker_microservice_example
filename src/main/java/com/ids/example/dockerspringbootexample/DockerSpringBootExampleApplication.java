package com.ids.example.dockerspringbootexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DockerSpringBootExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(DockerSpringBootExampleApplication.class, args);
	}
}
